import React, { Component } from 'react';
import './number.css';

class Numbers extends Component {
    render(){
        return (
            <div className="Number">
                 <span>{this.props.number}</span>
            </div>

        )
    }
};

export default Numbers;
