import React, { Component } from 'react';
import Numbers from './components/Numbers/number'
import './App.css';

class App extends Component {
    state = {
        number: []
    };
    newNumbers = ()  => {
      let numberCopy = [...this.state.number];
        const  numbers = [];
        while(numbers.length < 5) {
            let newNumber = Math.floor(Math.random() * (36 - 5) + 5);
            if(!numbers.includes(newNumber)) {
                numbers.push(newNumber);
            }
        }

      numberCopy = numbers.sort((a,b)=>{
        return a-b;
      });

      this.setState({
          number: numberCopy
      });
    };
  render() {
    return (
      <div className="App">
          <div>
              <button  className="btn" onClick={this.newNumbers}>New Number</button>
          </div>
          <h1 className="titleName">Loterry</h1>
          <Numbers number ={this.state.number[0]} >
          </Numbers>
          <Numbers number ={this.state.number[1]} >
          </Numbers>
          <Numbers number ={this.state.number[2]} >
          </Numbers>
          <Numbers number ={this.state.number[3]} >
          </Numbers>
          <Numbers number ={this.state.number[4]} >
          </Numbers>

      </div>
    );
  }
}

export default App;
